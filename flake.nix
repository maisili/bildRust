{
  description = "bildRust";

  outputs = { self }: {
    strok = {
      spici = "lamdy";
    };

    datom = uyrld @{ kor, gluCratesIO, stdenv, rustc, nightlyRust }:

    bildRustArgz@{
      neim,
      vyrzyn,
      iuniksSors,
      modzSet ? {},
      indeksCrates ? {},
      features ? [],
      spici ? "mod",
      nightly ? false,
      no_std ? false,
    }:

    let
      inherit (kor) attrsToList mapAttrsToList;
      inherit (builtins) concatMap hasAttr mapAttrs;

      oyntcekdModz = attrsToList bildRustArgz.modzSet;

      izNightly = set:
      hasAttr "nightly" set && set.nightly;

      tcekModzNightly = builtins.any (m: izNightly m) oyntcekdModz;

      nightly = if (izNightly bildRustArgz)
      then true else tcekModzNightly;

      enforsModNightly = mod: if (izNightly mod)
      then mod else mod.override { nightly = true; };

      nightlyModz = map enforsModNightly oyntcekdModz;

      modz = if nightly then nightlyModz else oyntcekdModz;

      hazModz = mod: hasAttr "modzSet" mod && (mod.modzSet != {});

      komplitModz = kor.unique (modz ++
      concatMap (m: m.komplitModz) modz);

      crates = gluCratesIO { inherit nightly; };

      matcCrateDep = neim:
      assert kor.mesydj (hasAttr neim crates)
      "Crate ${neim} not found";
      let
        overrides = kor.filterAttrs
        (n: v: builtins.elem n [ "features" ]) indeksCrates.${neim};
        crate = crates.${neim}.override overrides;
      in crate.lib;

      dependencies = map matcCrateDep (builtins.attrNames indeksCrates);

      completeDeps = kor.unique (dependencies ++
      concatMap (dep: dep.completeDeps) dependencies);

      mkLibString = libDir:
      "-L dependency=${libDir}";

      cratesLibz = map (crate: mkLibString "${crate.lib}/lib")
      completeDeps;

      modzLibz = map (mod: mkLibString mod.out) modz;

      depsLibz = cratesLibz ++ modzLibz;

      mkExternString = neim: path:
      "--extern ${neim}=${path}";

      mkModExtern = mod:
      mkExternString mod.name "${mod.out}${mod.ryzylt}";

      modzExternz = map mkModExtern modz;

      mkCrateExtern = crate:
      let
        normalizeName = builtins.replaceStrings ["-"] ["_"];
        crateExternName = normalizeName crate.libName;
        externPath = "${crate.lib}/lib/lib${crateExternName}.rlib";
        externString = mkExternString crateExternName externPath;
      in externString;

      cratesExternz = map mkCrateExtern dependencies;
      externz = cratesExternz ++ modzExternz;

      spiciDatom = {
        mod = {
          crateType = "lib";
          ryzylt = "/lib.rlib";
        };
        bin = {
          crateType = "bin";
          ryzylt = "/${neim}";
        };
      }.${spici};

      inherit (spiciDatom) crateType ryzylt;

      bildRyzyltDir = "bild/";
      bildRyzylt= "${bildRyzyltDir}/${ryzylt}";

      rustc = if nightly then nightlyRust.rust else uyrld.rustc;

      rustcInkanteicyn = builtins.concatStringsSep " " ([
        "${rustc}/bin/rustc" "--crate-name ${neim}" iuniksSors
        "--crate-type ${crateType}" "--edition 2018"
        "--remap-path-prefix=$NIX_BUILD_TOP=/"
        "-C codegen-units=$NIX_BUILD_CORES"
        "-o ${bildRyzylt}"
      ] ++ externz ++ depsLibz );

    in
    stdenv.mkDerivation {
      name = neim;
      version = vyrzyn;

      passthru = {
        inherit ryzylt nightly modz
        dependencies completeDeps;
      };

      phases = [ "configurePhase" "buildPhase" "installPhase" ];

      configurePhase = ''
        mkdir --parents ${bildRyzyltDir}
      '';

      buildPhase = rustcInkanteicyn;

      installPhase = ''
        mkdir --parents $out
        cp ${bildRyzylt} $out${ryzylt}
      '';

    };
  };
}
